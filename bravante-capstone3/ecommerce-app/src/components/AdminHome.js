import Landing from "./Landing";
import {Container} from "react-bootstrap"

export default function AdminHome() {
    return (
        <>
        <h3>Admin Dashboard</h3>
            <Container>            
                <Landing/>
            </Container>
        </>
    
    )

}